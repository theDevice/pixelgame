#include <iostream>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <unistd.h>     //sleep
#include <stdlib.h>     //srand, rand

using namespace std;

SDL_Surface *screen;
SDL_Event event;

#define SCALE 4
#define NUMBER_OF_COLORS 15
#define WIDTH 250
#define HEIGHT 180

uint32_t field[WIDTH][HEIGHT];      // this is a copy of the screen, because i couldn't figure out how to read directly a pixels color in a normal format

#define BPP 4
#define DEPTH 32



bool setPixel_real(int in_x, int in_y, int in_color){
    bool output = false;

    if( in_x < 0 || in_x >= WIDTH*SCALE || in_y < 0 || in_y >= HEIGHT*SCALE ){
        cout << "setPixel_real(): area invalid!: x: " << in_x << " ; y: " << in_x << endl;
        return output;
    }

    int color = 0;

    Uint32 *foo;
	switch(in_color) {
        case 0:
            color = SDL_MapRGB( screen->format, 0, 0, 0);
        break;
        case 1:
            color = SDL_MapRGB( screen->format, 0, 0, 255);
        break;
        case 2:
            color = SDL_MapRGB( screen->format, 0, 139, 0);
        break;
        case 3:
            color = SDL_MapRGB( screen->format, 0, 255, 255);
        break;
        case 4:
            color = SDL_MapRGB( screen->format, 255, 0, 0);
        break;
        case 5:
            color = SDL_MapRGB( screen->format, 255, 0, 255);
        break;
        case 6:
            color = SDL_MapRGB( screen->format, 165, 42, 42);
        break;
        case 7:
            color = SDL_MapRGB( screen->format, 240, 240, 240);
        break;
        case 8:
            color = SDL_MapRGB( screen->format, 190, 190, 190);
        break;
        case 9:
            color = SDL_MapRGB( screen->format, 173, 216, 230);
        break;
        case 10:
            color = SDL_MapRGB( screen->format, 0, 255, 0);
        break;
        case 11:
            color = SDL_MapRGB( screen->format, 224, 255, 255);
        break;
        case 12:
            color = SDL_MapRGB( screen->format, 255, 192, 203);
        break;
        case 13:
            color = SDL_MapRGB( screen->format, 139, 0, 139);
        break;
        case 14:
            color = SDL_MapRGB( screen->format, 255, 255, 0);
        break;
        case 15:
            color = SDL_MapRGB( screen->format, 255, 255, 255);
        break;
        default:
            //in_color = SDL_MapRGB( screen->format, 0, 0, 0);
            cout << "someting wrong: input: x: " << in_x << " y: " << in_y << " color: " << in_color << endl;
            exit(1);
        break;
    };
	foo = (Uint32*) screen -> pixels + ( in_y * screen -> pitch / BPP ) + ( in_x );
    *foo = color;
    output = true;



    return output;
}


bool setPixel_unscaled( int in_x, int in_y, int in_color ){
    bool output = false;

    //cout << "setPixel_unscaled(): in_x: " << in_x << "\tin_y : " << in_y << "\tin_color: " << in_color << endl;

    if( in_x < 0 || in_x >= WIDTH || in_y < 0 || in_y >= HEIGHT ){
        cout << "setPixel_unscaled(): area invalid!: x: " << in_x << " ; y: " << in_x << endl;
        return output;
    }

    field[in_x][in_y] = in_color;
    int new_x = in_x * SCALE;
    int new_y = in_y * SCALE;
    for( int delta_x = 0; delta_x < SCALE; delta_x++){
        for( int delta_y = 0; delta_y <SCALE; delta_y++ ){
            setPixel_real( new_x+delta_x, new_y+delta_y, in_color );
        }
    }
    output = true;
    return output;

}

void initGraph (){
	if (SDL_Init(SDL_INIT_VIDEO)<0) printf("SDL_Init failed\n");
    if (!(screen = SDL_SetVideoMode(WIDTH*SCALE, HEIGHT*SCALE, DEPTH, SDL_HWSURFACE))){
    	printf("SDL_SetVideoMode failed");
        SDL_Quit();
    }
    printf("\ngrafic init!\n");
}


void initField(){
    for(int x = 0; x < WIDTH; x++){
        for(int y = 0; y < HEIGHT; y++){
            field[x][y] = 0;
        }
    }
}

void lockScreen(){
	if(SDL_MUSTLOCK(screen))    {
        if(SDL_LockSurface(screen) < 0)
        return;
    }
}

void unlockScreen(){
	if(SDL_MUSTLOCK(screen)) SDL_UnlockSurface(screen);
    SDL_Flip(screen);
}

void closeGraph(){
    SDL_Quit();
}

/*!
void clearScreen(){
    int i, j;
    for (i = 0; i < WIDTH; i++){
        for (j = 0; j < HEIGHT; j++){

            setPixel(i, j, 0);

        }
    }
}
*/

int getPixel( int in_x, int in_y ){
    return field[in_x][in_y];
}

void setRandomColors( int in_howManyColors ){
    lockScreen();
    for( int x = 0; x < WIDTH; x++ ){
        for( int y = 0; y < HEIGHT; y++ ){
            int color = rand() % in_howManyColors;
            setPixel_unscaled(x, y, color);
        }
    }
    unlockScreen();
    cout << "leaving setRandomColors()" << endl;
}

int main(){
    // init random seed
    srand (time(NULL));

    // init
    initGraph();
    initField();

    setRandomColors(NUMBER_OF_COLORS);
    while( 42 == 42 ){

        int x = rand() % WIDTH;
        int y = rand() % HEIGHT;
        int color = getPixel(x, y);

        lockScreen();

        setPixel_unscaled( x-1, y-1, color);
        setPixel_unscaled( x, y-1, color);
        setPixel_unscaled( x+1, y-1, color);
        setPixel_unscaled( x-1, y, color);
        setPixel_unscaled( x+1, y, color);
        setPixel_unscaled( x-1, y+1, color);
        setPixel_unscaled( x, y+1, color);
        setPixel_unscaled( x+1, y+1, color);

        unlockScreen();

    }
    closeGraph();
    return 0;
}
